import { BarCodeScanner } from "expo-barcode-scanner";
import * as Notifications from 'expo-notifications';
import { StatusBar } from "expo-status-bar"
import { useEffect, useState } from "react"
import { Button, StyleSheet, Text, View, Platform, TextInput } from "react-native"
import * as Device from 'expo-device';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: true    
    }),
  });
  

 export const QRScanner  = () => {
    const [hasPermition, setHasPermition] = useState(false);
    const [scanned, setScanned] = useState(false);
    const [text, setText] = useState('NADA');
    const [expoPushToken, setExpoPushToken] = useState('');
    const [notification, setNotification] = useState('');
    

    const askForCameraPermision = () =>  {
        (async () => {
               const {status} = await BarCodeScanner.requestPermissionsAsync();
               setHasPermition(status === 'granted');
           }
        )()
    }

    useEffect(() => {
      askForCameraPermision();
      registerForPushNotificationsAsync().then((token: any) => setExpoPushToken(token));
      Notifications.addNotificationReceivedListener(_handleNotification);
      Notifications.addNotificationResponseReceivedListener(_handleNotificationResponse);
    }, []);


    const _handleNotification = (notification:any) => {
            setNotification(notification);
    };
    
    const _handleNotificationResponse = (response:any) => {
        console.log(response);
      };

    const handleBarCodeScanned = ({type,data}: any) => {
        setScanned(true);
        setText(data);

        console.log('type ' + type + ' data ' + data);
    }


    async function registerForPushNotificationsAsync() {
        console.log('Entro');
        let token;
        if (Device.isDevice) {
          const { status: existingStatus } = await Notifications.getPermissionsAsync();
          let finalStatus = existingStatus;
          if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
          }
          if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
          }
          token = (await Notifications.getDevicePushTokenAsync()).data;
          console.log(token);
        } else {
          alert('Must use physical device for Push Notifications');
        }
      
        if (Platform.OS === 'android') {
          Notifications.setNotificationChannelAsync('default', {
            name: 'default',
            importance: Notifications.AndroidImportance.MAX,
            vibrationPattern: [0, 250, 250, 250],
            lightColor: '#FF231F7C',
          });
        }
      
        return token;
      }
    
    if ( hasPermition === false) {
        return (
            <>
                <View style={styles.container} >
                    <Text>Requerimos permisos</Text>
                    <StatusBar></StatusBar>
                </View>
            </>
        )
    }

    return (
        <>
            <View style={styles.container} >
            <Text>Your expo push token: {expoPushToken}</Text>


                <View style={styles.barCodeBox} >
                    <BarCodeScanner 
                        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} 
                        style={styles.barCode}
                    />
                </View>
                <Text style={styles.mainText}>{text}</Text>
                {scanned && <Button title={'Scaned again?'} onPress={() => setScanned(false)} color='tomato'></Button>}


                <TextInput
        value={expoPushToken}
      />
            </View>
        </>
    )
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    barCodeBox: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 300,
        width: 300,
        overflow: 'hidden',
        borderRadius: 30,
        backgroundColor: 'tomato',
    },
    barCode: {
        height: 200,
        width: 200
    },
    mainText: {
        fontSize: 16,
        margin: 20,
    }
})