import React, { useEffect, useState } from "react";
import { Text, View,StyleSheet, Image, Button, Alert, TouchableOpacityBase, TouchableOpacity, Platform, ScrollView } from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Sharing from 'expo-sharing';
import { getPermissionsAsync } from "expo-notifications";
import * as Calendar from 'expo-calendar';
import { QRScanner } from "./pages/QRSanner";


const App = () => {
  const [selectImage, setSelectImage] = useState({localUri : null});
  const [selectImageCamera, setSelectImageCamera] = useState({localUri: null});

  useEffect(() => {
    (async () => {
      const { status } = await Calendar.requestCalendarPermissionsAsync();
      if (status === 'granted') {
        const calendars = await Calendar.getCalendarsAsync(Calendar.Frequency.MONTHLY);
        const events = await Calendar.getCalendarsAsync('20');
      }
    })();
  }, []);

  let getToken = async () => {
      const permition = await getPermissionsAsync();
      console.log('permition',permition);
  }

  let openImagePickerAsync = async () => {
      let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

      if (permissionResult.granted === false) {
        Alert.alert('Permission to access galery is required');
        return;
      }

      const pickerResult: any = await ImagePicker.launchImageLibraryAsync();

      if (pickerResult.cancelled === true) {
        return;
      }

      setSelectImage({localUri : pickerResult.uri});

  }


  let openImageCameraAsync = async () => {
    let permissionResult = await ImagePicker.requestCameraPermissionsAsync();
    if (permissionResult.granted === false) {
      Alert.alert('Permission to access camera is required');
      return;
    }
    const pickerResult: any = await ImagePicker.launchCameraAsync();
    
    if (pickerResult.cancelled === true) {
      return;
    }
    setSelectImageCamera({localUri : pickerResult.uri});
    
  }

  let shared = async () => {
    let isAvailable: boolean = await Sharing.isAvailableAsync();
    if (!isAvailable) {
      Alert.alert('Sharing is not available');
      return;
    }
    if (selectImageCamera.localUri) {
      await Sharing.shareAsync(selectImageCamera.localUri);
    }
  }

  async function getDefaultCalendarSource() {
    const defaultCalendar = await Calendar.getDefaultCalendarAsync();
    return defaultCalendar.source;
  }

  let deleteCalendar = async () => {
    Calendar.deleteCalendarAsync('20').then(r => {
      console.log('response delete',r);
      
    })
  }


  async function createCalendar() {
    const defaultCalendarSource: any = Platform.OS === 'ios' ? await getDefaultCalendarSource(): {};
    Calendar.createCalendarAsync({
      title: 'Rony calendar',
      color: 'red',
      entityType: Calendar.EntityTypes.EVENT,
      sourceId: defaultCalendarSource.id,
      source: defaultCalendarSource,
      accessLevel: Calendar.CalendarAccessLevel.OWNER,
    }).then(newCalendarID => {

      let dateMs = Date.parse('2022/03/18')
      let startDate = new Date(dateMs)
      let endDate = new Date(dateMs + 2 * 60 * 60 * 1000)
      Calendar.createEventAsync(newCalendarID, {
        title: 'TEST RONY',
        startDate,
        endDate,
        timeZone: 'America/Mexico_City'
      })
    })

    
  }

  return (
    <ScrollView>
    <View style={styles.container}>
      <Text style={styles.text}>Hello</Text>

        <Button
          onPress={openImagePickerAsync}
          title= 'Galery'
        />

        <Image
          source={{uri: selectImage.localUri !== null ? selectImage.localUri : 'https://picsum.photos/200/200'}}
          style={styles.image}
        />
         <Button
          onPress={openImageCameraAsync}
          title= 'Camera'
        />
        <TouchableOpacity onPress={shared}>
          <Image
            source={{uri: selectImageCamera.localUri !== null ? selectImageCamera.localUri : 'https://picsum.photos/200/200'}}
            style={styles.image}
          />
        </TouchableOpacity>


        <Button
          onPress={createCalendar}
          title= 'CREAR CALENDARIO'
        />
        <Button
          onPress={deleteCalendar}
          title= 'DELETE CALENDAR'
        />

        <QRScanner/>


    </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 50,
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50,
  }
})


export default App;